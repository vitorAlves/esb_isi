var express = require('express');
var router = express.Router();
var req = require('request');
var async = require('async');
var soap = require('soap');
var moment = require('moment');

var mainModel = require('../models/index.model.js');



router.get('/', function (request, response) {
    //console.log(request.isAuthenticated());

    mainModel.getCredentials((err, rows) => {
        if (err) console.log(err);
        var data = [{
            access_token: rows.access_token,
            refresh_token: rows.refresh_token,
            access_token_jasmin: rows.jasmin_access_token
        }]

        response.set('Content-Type', 'application/json');
        response.send(data);

    });
});



router.post('/purchase1', function (request, response) {

    var url = 'http://10.8.0.2:7047/DynamicsNAV110/WS/CRONUS%20International%20Ltd./Page/SalesOrder';
    var auth = "Basic " + new Buffer("Hugo" + ":" + "Isi20199").toString("base64");

    var args = {
        username: 'Hugo',
        password: 'Isi20199'
    };
    var SalesOrder = {
        Sell_to_Customer_No: 'C00010',
        SalesLines: {
            Sales_Order_Line: {

                Type: 'Item',
                No: '70072',
                Description: 'Bounty',
                Quantity: 12,
                Unit_Price: 2
            }

        }
    }
    soap.createClient(url, {
        wsdl_headers: {
            Authorization: auth
        }
    }, function (err, client) {

        if (err) console.log(err);
        client.setSecurity(new soap.BasicAuthSecurity('Hugo', 'Isi20199'));
        client.Create({
            SalesOrder
        }, function(err, result) {
            response.set("Content-Type", "application/json");
            if (err) {
                response.send('fds' + err);
            } else {
                response.send(result);
            }
        });
    });

})


router.post('/purchase', function (request, response) {


    var url = 'http://10.8.0.2:7047/DynamicsNAV110/WS/CRONUS%20International%20Ltd./Page/SalesOrder';
    var auth = "Basic " + new Buffer("Hugo" + ":" + "Isi20199").toString("base64");

    var args = {
        username: 'Hugo',
        password: 'Isi20199'
    };
    soap.createClient(url, {
        wsdl_headers: {
            Authorization: auth
        }
    }, function (err, client) {
        client.setSecurity(new soap.BasicAuthSecurity('Hugo', 'Isi20199'));
        client.Create({}, function (err, result, rawResponse, soapHeader, rawRequest) {
            console.log(err);
            console.log(rawRequest);
        });
    });

})



router.post('/getProductsMachine', function (request, response) {

    async.waterfall([

            (done) => {

                let companyId = request.body.companyId;
                let warehouseId = request.body.warehouseId;
                mainModel.getCredentials((err, rows) => {

                    if (err) console.log("mi+W");

                    done(err, rows.access_token, companyId, warehouseId);
                });

            },

            (access, companyId, warehouseId, done) => {

                var data = {
                    url: `https://api.moloni.pt/v1/products/getAll/?access_token=${access}&json=true`,

                    json: {
                        company_id: companyId,
                        category_id: 761162

                    }
                }


                req.post(data, (err, body, res) => {


                    console.log(res);
                    done(null, res);

                })



            }


        ],
        function (err, result) {

            console.log('done');
            response.set("Content-Type", "application/json");

            if (err) {
                response.send('fds' + err);
            } else {
                response.send(result);
            }
        });
});



router.post('/insertStock', function (request, response) {

    async.waterfall([

            (done) => {

                let companyId = request.body.companyId;
                mainModel.getCredentials((err, rows) => {

                    if (err) console.log("mi+W");

                    done(err, rows.access_token, companyId);
                });
            },
            (access, companyId, done) => {

                console.log(companyId);

                var options = {
                    url: `https://api.moloni.pt/v1/supplierInvoices/insert/?access_token=${access}&json=true`,
                    json: {
                        company_id: request.body.companyId,
                        date: moment().format(),
                        expiration_date: moment().format(),
                        document_set_id: 119293,
                        supplier_id: request.body.supplier,
                        associated_documents: [],
                        status: 1,
                        products: [{
                            product_id: request.body.id_product,
                            name: request.body.name,
                            summary: "",
                            qty: request.body.quantity,
                            price: request.body.price,
                            discount: 0,
                            order: 0,

                            warehouse_id: request.body.warehouse,
                            taxes: [{
                                tax_id: 591713,
                                exemption_reason: ""
                            }],

                        }],

                    }
                }
                console.log(JSON.stringify(options, 0, 3));
                console.log(JSON.stringify(typeof request.body.productid));
                if (companyId) {
                    console.log('got here machine');
                    req.post(options, (err, body, res) => {
                        console.log('err' + err);

                        var prettyRes = res;
                        done(err, prettyRes);

                    })

                } else {
                    done(null, {
                        error: 'wrong Arguments'
                    });


                }
            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send('fds' + err);
            } else {
                response.send(result);
            }
        });
});


router.post('/moveStock', function (request, response) {


    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, rows.access_token);
                });



            },
            (access, done) => {

                var options = {
                    url: `https://api.moloni.pt/v1/productStocks/insert/?access_token=${access}&json=true&human_errors=true`,
                    json: {
                        company_id: request.body.companyId,
                        product_id: request.body.productId,
                        movement_date: moment().format(),
                        qty: request.body.quantity,
                        warehouse_id: request.body.arehouseId

                    }
                }

                console.log(JSON.stringify(options, 0, 3));

                req.post(options, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })


            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});




router.post('/sellProducts', function (request, response) {


            async.waterfall([

                    (done) => {

               let companyId = request.body.companyId;
                let warehouseId = request.body.warehouseId;
                let productId = request.body.productId;
                mainModel.getCredentials((err, rows) => {

                    if (err) console.log("mi+W");

                    done(err, rows.access_token, companyId, warehouseId, productId);
                });

            },
            (access, companyId, warehouseId, productId, done) => {

                var options = {
                    url: `https://api.moloni.pt/v1/productStocks/getAll/?access_token=${access}&json=true`,
                    json: {
                        company_id: companyId,
                        warehouse_id: warehouseId,
                        product_id: productId
                    }
                }

                if (companyId) {
                    console.log('got here machine');
                    req.post(options, (err, body, res) => {
                        console.log('err' + err);
                        console.log(res);
                        var prettyRes = res;
                        done(err, access);

                    })

<<<<<<< HEAD
                    },
                    (access, done) => {

                        let products = [];
=======
                } else {
                    done(null, access, {
                        error: 'wrong Arguments'
                    });
                }
            },

            (access, done) => {
                var options = {
                    url: `https://api.moloni.pt/v1/simplifiedInvoices/insert/?access_token=${access}&json=true&human_errors=true`,
                    json: {
                        company_id: request.body.companyId,
                        date: moment().format(),
                        expiration_date: moment().format(),
                        document_set_id: 119293,
                        customer_id: 10824383,
                        associated_documents: [],
                        status: 1,

                        for (var e = 0; e < request.body.length; e++) {

                            products.push({
                                product_id: request.body[e].id_product,
                                name: request.body[e].product_name,
                                summary: "",
                                qty: request.body[e].quantity,
                                price: request.body[e].product_price,
                                discount: 0,
                                order: 0,

                                warehouse_id: request.body[e].warehouse,
                                taxes: [{
                                    tax_id: 591713,
                                    exemption_reason: ""

                                }]
                            });

                            var options = {
                                url: `https://api.moloni.pt/v1/simplifiedInvoices/insert/?access_token=${access}&json=true`,
                                json: {
                                    company_id: request.body[0].companyId,
                                    date: moment().format(),
                                    expiration_date: moment().format(),
                                    document_set_id: 119293,
                                    customer_id: 10824383,
                                    associated_documents: [],
                                    status: 1,
                                    products,




                                    payments: [{
                                        payment_method_id: 402488,
                                        date: moment().format(),
                                        value: request.body[e].product_price * request.body[e].quantity
                                    }]
                                }
                            }
                            console.log(JSON.stringify(options, 0, 3));


                                req.post(options, (err, body, res) => {

                                    var prettyRes = res;
                                    done(err, prettyRes);

<<<<<<< HEAD
                                })
=======
        ],
        function (err, result) {
>>>>>>> 51f083434e3ff179dbff2e409d9f97660d62be14


                            }
                        }

                    ],
                    function(err, result) {

<<<<<<< HEAD
                        response.set("Content-Type", "application/json");

                        if (err) {
                            response.send(err);
                        } else {
                            response.send(result);
                        }
                    });
            });

=======
router.post('/criarFaturaJasmin', function (request, response) {
    async.waterfall([
>>>>>>> 51f083434e3ff179dbff2e409d9f97660d62be14

        router.post('/criarFaturaJasmin', function(request, response) {

<<<<<<< HEAD

            async.waterfall([

                    (done) => {

                        mainModel.getCredentials((err, rows) => {
                            if (err) console.log(err);
                            done(err, rows.access_token_jasmin);
                        });



=======
                mainModel.getCredentials((err, rows) => {

                    if (err) console.log("mi+W");

                    done(err, rows.access_token_jasmin);
                });

            },

            (access, done) => {


                var options = {
                    url: 'https://my.jasminsoftware.com/api/219113/219113-0003/billing/invoices/',
                    header: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Bearer': access
>>>>>>> 51f083434e3ff179dbff2e409d9f97660d62be14
                    },
                    (access, done) => {

                        var options = {
                            url: `https://my.jasminsoftware.com/api/219113/219113-0003/billing/invoices/`,
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            auth: {
                                user: 'myAppISI',
                                pass: '217740af-501a-438f-ac7b-16816dbc6d2f'
                            },

<<<<<<< HEAD
                            formData: {
                                client_id: 'myAppISI',
                                client_secret: '217740af-501a-438f-ac7b-16816dbc6d2f',
                                grant_type: 'client_credentials',
                                scope: 'application'
                            },

                            json: {
                                documentType: "FA",
                                serie: "2019",
                                seriesNumber: 1,
                                company: "Default",
                                paymentTerm: "01",
                                paymentMethod: "NUM",
                                currency: "EUR",
                                documentDate: moment().format(),
                                postingDate: moment().format(),
                                buyerCustomerParty: "Cliente",
                                buyerCustomerPartyName: "ISI_UM",
                                accountingPartyName: "ISI_UM",
                                accountingPartyTaxId: "593362462",
                                exchangeRate: 1,
                                discount: 0,
                                loadingCountry: "PT",
                                unloadingCountry: "PT",
                                isExternal: false,
                                isManual: false,
                                isSimpleInvoice: false,
                                isWsCommunicable: false,
                                deliveryTerm: "V-VIATURA",
                                documentLines: [{
                                    salesItem: request.body.item,
                                    description: request.body.descrip,
                                    quantity: request.body.qty,
                                    unitPrice: {
                                        amount: 65,
                                        baseAmount: 65,
                                        reportingAmount: 65,
                                        fractionDigits: 2,
                                        symbol: "€"
                                    },
                                    unit: "UN",
                                    itemTaxSchema: "IVA-TN",
                                    deliveryDate: "moment().format()"
                                }],
                                WTaxTotal: {
                                    amount: 0,
                                    baseAmount: 0,
                                    reportingAmount: 0,
                                    fractionDigits: 2,
                                    symbol: "€"
                                },

                                TotalLiability: {
                                    baseAmount: 0,
                                    reportingAmount: 0,
                                    fractionDigits: 2,
                                    symbol: "€"
                                }
                            }
=======
                }
                req.post(options, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })

            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});

router.post('/sendEncomendaHana', function (request, response) {


    async.waterfall([

            (done) => {

                var options = {
                    url: `https://encomendap2001333248trial.hanatrial.ondemand.com/hanarest-1.0/encomenda`,
                    json: {
                        id_maquina: request.body.id_maquina,
                        id_companhia: 69782,
                        id_artigo: request.body.id_artigo,
                        quantidade: request.body.quantidade,

                    },

                }

                        }

                        console.log(JSON.stringify(options, 0, 3));

                        req.post(options, (err, body, res) => {

                            var prettyRes = res;
                            done(err, prettyRes);
            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});

router.post('/sendRotaHana', function (request, response) {


    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,
                    json: {
                        id_maquina: request.body.id_maquina,
                        id_companhia: 69782,
                        latitude: request.body.latitude,
                        longitude: request.body.longitude,
                        id_artigo: request.body.id_artigo,
                        quantidade: request.body.quantidade,
                        estado: request.body.estado,
                    },
                }
                req.post(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })

            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});

router.post('/getEncomendaHana', function (request, response) {


    async.waterfall([

            (done) => {

                var options = {
                    url: `https://encomendap2001333248trial.hanatrial.ondemand.com/hanarest-1.0/encomenda`,
                    json: {
                        id_maquina: request.body.id_maquina,
                        id_companhia: 69782,
                        id_artigo: request.body.id_artigo,
                        quantidade: request.body.quantidade,

                    },

                }

                req.get(options, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })
>>>>>>> 51f083434e3ff179dbff2e409d9f97660d62be14

                        })

<<<<<<< HEAD
=======
        ],
        function (err, result) {
>>>>>>> 51f083434e3ff179dbff2e409d9f97660d62be14

                    }

                ],
                function(err, result) {

                    response.set("Content-Type", "application/json");

                    if (err) {
                        response.send(err);
                    } else {
                        response.send(result);
                    }
                });
        });

router.post('/getRotaHana', function (request, response) {


    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,
                    json: {
                        id_maquina: request.body.id_maquina,
                        id_companhia: 69782,
                        latitude: request.body.latitude,
                        longitude: request.body.longitude,
                        id_artigo: request.body.id_artigo,
                        quantidade: request.body.quantidade,
                        estado: request.body.estado,
                    },
                }
                req.get(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })

            }

        ],
        function (err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});

module.exports = router;
