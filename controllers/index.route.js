var express = require('express');
var router = express.Router();
var req = require('request');
var async = require('async');
var soap = require('soap');
var moment = require('moment');

var mainModel = require('../models/index.model.js');


var url = "https://replica.eupago.pt/clientes/rest_api";
var chave = "demo-e748-4743-4c52-7b2";


router.get('/', function(request, response) {

    mainModel.getCredentials((err, rows) => {
        var data = [{
            access_token: rows.access_token,
            refresh_token: rows.refresh_token,
            access_token_jasmin: rows.jasmin_access_token
        }]

        response.set('Content-Type', 'application/json');
        response.send(data);

    });
});


function purchaseNAV(options, callback) {

    var url = 'http://10.8.0.4:7047/DynamicsNAV110/WS/CRONUS%20International%20Ltd./Page/SalesOrder';
    var auth = "Basic " + new Buffer("Hugo" + ":" + "Isi20199").toString("base64");

    let SalesLines = [];
    for (var e of options) {
        let Sales_Order_Line = {
            Sales_Order_Line: {
                Type: 'Item',
                No: e.id_artigo,
                Description: e.nome,
                Quantity: e.quantidade,
                Unit_Price: 0.20
            },

        }
        console.log("fsd");
        SalesLines.push(Sales_Order_Line);


    }


    console.log("SalesLines! 1" + JSON.stringify(SalesLines, 0, 3));
    var args = {
        username: 'Hugo',
        password: 'Isi20199'
    };
    var SalesOrder = {
        Sell_to_Customer_No: 'C00010',
        SalesLines: [
            SalesLines
        ]

    }
    console.log("SalesOrder! 2" + JSON.stringify(SalesOrder, 0, 3));

    soap.createClient(url, {
        wsdl_headers: {
            Authorization: auth
        }
    }, function(err, client) {


        client.setSecurity(new soap.BasicAuthSecurity('Hugo', 'Isi20199'));

        console.log("SalesLines! ENding " + JSON.stringify(SalesOrder, 0, 3));
        client.Create({
            SalesOrder
        }, function(err, result) {
            console.log(err)
            console.log(result)
            callback(err, result)
        });
    });




}


router.post('/purchase', function(request, response) {

    var url = 'http://10.8.0.4:7047/DynamicsNAV110/WS/CRONUS%20International%20Ltd./Page/SalesOrder';
    var auth = "Basic " + new Buffer("Hugo" + ":" + "Isi20199").toString("base64");

    var args = {
        username: 'Hugo',
        password: 'Isi20199'
    };
    var SalesOrder = {
        Sell_to_Customer_No: 'C00010',
        SalesLines: {
            Sales_Order_Line: {

                Type: 'Item',
                No: '70072',
                Description: 'Bounty',
                Quantity: 12,
                Unit_Price: 2
            }

        }
    }
    soap.createClient(url, {
        wsdl_headers: {
            Authorization: auth
        }
    }, function(err, client) {
        if (err) console.log(err);
        client.setSecurity(new soap.BasicAuthSecurity('Hugo', 'Isi20199'));
        client.Create({
            SalesOrder
        }, function(err, result) {
            response.set("Content-Type", "application/json");

            if (err) {
                response.send('fds' + err);
            } else {
                response.send(result);
            }
        });
    });

})




router.post('/getProductsMachine', function(request, response) {

    async.waterfall([

            (done) => {

                let companyId = request.body.companyId;
                let warehouseId = request.body.warehouseId;
                mainModel.getCredentials((err, rows) => {


                    done(err, rows.access_token, companyId, warehouseId);
                });

            },

            (access, companyId, warehouseId, done) => {

                var data = {
                    url: `https://api.moloni.pt/v1/products/getAll/?access_token=${access}&json=true`,

                    json: {
                        company_id: companyId,
                        category_id: 761162

                    }
                }



                req.post(data, (err, body, res) => {


                    console.log(res);

                    for (var e of res) {

                        let stocks;
                        for (var i of e.warehouses) {
                            if (i.warehouse_id == warehouseId) {

                                stocks = i.stock;

                            }


                        }
                        e.stock = stocks;
                    }
                    done(null, res);

                })



            }


        ],
        function(err, result) {

            console.log('done');
            response.set("Content-Type", "application/json");

            if (err) {
                response.send('fds' + err);
            } else {
                response.send(result);
            }
        });
});



function insertProducts(options, callback) {

    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, options, rows.access_token);
                });
            },

            (options, access, done) => {

                console.log('options' + JSON.stringify(options, 0, 3));
                let products = [];
                for (var e = 0; e < options.length; e++) {

                    products.push({
                        product_id: options[e].id_product,
                        name: options[e].product_name,
                        summary: "",
                        qty: options[e].quantity,
                        price: options[e].product_price,
                        discount: 0,
                        order: 0,

                        warehouse_id: options[e].warehouse,
                        taxes: [{
                            tax_id: 591713,
                            exemption_reason: ""

                        }]
                    })
                }
                var optionsReq = {
                    url: `https://api.moloni.pt/v1/supplierInvoices/insert/?access_token=${access}&json=true`,
                    json: {
                        company_id: options[0].companyId,
                        date: moment().format(),
                        expiration_date: moment().format(),
                        document_set_id: 119293,
                        customer_id: 10824383,
                        associated_documents: [],
                        status: 1,
                        products
                    }
                }

                checkStock(optionsReq, access, (err, res) => {

                    console.log(err);
                    console.log(res);
                    if (err) {
                        done(err, res);
                    } else {
                        console.log('somehow got here' + err + res);

                        req.post(optionsReq, (err, body, res) => {

                            console.log('dammit' + JSON.stringify(options, 0, 3));
                            var prettyRes = res;
                            done(err, prettyRes);

                        })
                    }
                });
            }
        ],
        function(err, result) {
            callback(err, result);
        });
}


router.post('/insertStock', function(request, response) {

    insertProducts(request.body, (err, res) => {
        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(res);
        }
    });
});


router.post('/moveStock', function(request, response) {

    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, rows.access_token);
                });



            },
            (access, done) => {

                var options = {
                    url: `https://api.moloni.pt/v1/productStocks/insert/?access_token=${access}&json=true&human_errors=true`,
                    json: {
                        company_id: request.body.companyId,
                        product_id: request.body.productId,
                        movement_date: moment().format(),
                        qty: request.body.quantity,
                        warehouse_id: request.body.warehouseId

                    }
                }

                console.log(JSON.stringify(options, 0, 3));

                req.post(options, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })


            }

        ],
        function(err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});


function checkStock(options, access, callback) {
    let adress = `https://api.moloni.pt/v1/products/getOne/?access_token=${access}&json=true`

    let err;
    let products = options.json.products;

    let i = 0;
    products.forEach(async e => {

        i = i + 1;
        let optionsStock = {
            url: adress,
            json: {
                company_id: options.json.company_id,
                product_id: e.product_id
            }
        }

        console.log("options " + JSON.stringify(options, 0, 3));
        req.post(optionsStock, (err, body, res) => {
            console.log(err);
            console.log(res);

            let quantity;
            let warehouse_id;

            let master_warehouse = 7737;
            let minimum;
            if (e.warehouse_id == master_warehouse) {

                minimum = 50;
            } else {
                minimum = 5;
            }
            for (var warehouse of res.warehouses) {

                if (warehouse.warehouse_id == e.warehouse_id) {
                    quantity = warehouse.stock;
                    warehouse_id = warehouse.warehouse_id
                }
            }
            if (quantity < e.qty) {
                console.log('no stock');
                err = `no stock on ${e.name}`;
                callback(err, 'yup');
            }
            if (quantity - e.qty < minimum) {

                if (warehouse_id == master_warehouse) {
                    console.log('a encomendar nav');

                    let order_quantity = 100 - quantity;

                    console.log('need ' + order_quantity + ' un');

                    let encomendaHana = {
                        id_artigo: e.product_id,
                        quantidade: order_quantity,
                        id_maquina: master_warehouse,
                    }
                    sendEncomendaHana(encomendaHana, (err, result) => {
                        console.log('Encomenda Hana ' + err + result);

                    });
                } else {

                    let order_quantity = 10 - quantity;

                    console.log('mover stock');
                    for (var warehouseMaster of res.warehouses) {

                        if (warehouseMaster.warehouse_id == master_warehouse) {

                            let available_master = warehouseMaster.stock;

                            if (available_master >= order_quantity) {

                                console.log('pode fazer a encomenda');
                                let encomendaHana = {
                                    id_artigo: e.product_id,
                                    quantidade: order_quantity,
                                    id_maquina: warehouse_id,
                                    id_companhia: 69782,
                                    latitude: 0,
                                    longitude: 0,
                                    estado: 0
                                }
                                sendRotaHana(encomendaHana, (err, result) => {
                                    console.log('Encomenda Hana ' + err + result);

                                });
                            } else {
                                console.log('pode fazer a encomenda Hana');
                                let encomendaHana = {
                                    id_artigo: e.product_id,
                                    quantidade: 100 - available_master,
                                    id_maquina: master_warehouse,
                                    id_companhia: 69782,
                                }
                                sendEncomendaHana(encomendaHana, (err, result) => {
                                    console.log('Encomenda Hana ' + err + result);

                                });




                            }

                        }



                    }




                }
            }

            if (i <= products.length && !err) {
                console.log('done');
                callback(null, 'done');

            }
        })

    })
}


function sellProducts(options, callback) {

    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, options, rows.access_token);
                });
            },

            (options, access, done) => {

                console.log('options' + JSON.stringify(options, 0, 3));
                let products = [];
                let value = 0;
                for (var e = 0; e < options.length; e++) {

                    products.push({
                        product_id: options[e].id_product,
                        name: options[e].product_name,
                        summary: "",
                        qty: options[e].quantity,
                        price: options[e].product_price,
                        discount: 0,
                        order: 0,

                        warehouse_id: options[e].warehouse,
                        taxes: [{
                            tax_id: 591713,
                            exemption_reason: ""

                        }]
                    });

                    value = value + options[e].quantity * options[e].product_price;
                    console.log(value);
                }
                var optionsReq = {
                    url: `https://api.moloni.pt/v1/simplifiedInvoices/insert/?access_token=${access}&json=true`,
                    json: {
                        company_id: options[0].companyId,
                        date: moment().format(),
                        expiration_date: moment().format(),
                        document_set_id: 119293,
                        customer_id: 10824383,
                        associated_documents: [],
                        status: 1,
                        products,




                        payments: [{
                            payment_method_id: 402488,
                            date: moment().format(),
                            value: value
                        }]
                    }
                }

                checkStock(optionsReq, access, (err, res) => {

                    console.log(err);
                    console.log(res);
                    if (err) {
                        done(err, res);
                    } else {
                        console.log('somehow got here' + err + res);

                        req.post(optionsReq, (err, body, res) => {

                            console.log('dammit' + JSON.stringify(options, 0, 3));
                            var prettyRes = res;
                            done(err, prettyRes);

                        })
                    }
                });
            }
        ],
        function(err, result) {
            callback(err, result);
        });
}

router.post('/sellProducts', function(request, response) {

    sellProducts(request.body, (err, res) => {
        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(res);
        }
    });
});




function faturaJasmin(options, callback) {

    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, options, rows.jasmin_access_token);
                });
            },

            (options, access, done) => {

                console.log('options' + JSON.stringify(options, 0, 3));
                let documentLines = [];
                for (var e of options) {

                    documentLines.push({
                        salesItem: e.nome,
                        quantity: e.qty,
                        description: "Chocolate",
                        summary: "",
                        unit: "UN",
                        itemTaxSchema: "IVA-TN",
                        unitPrice: {
                            amount: 0.2
                        },
                        deliveryDate: "moment().format()",
                        documentLineStatus: 2
                    });

                }
                var optionsReq = {
                    url: `https://my.jasminsoftware.com/api/219113/219113-0004/billing/invoices/`,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    auth: {
                        bearer: access,
                    },

                    json: {
                        documentType: "FA",
                        serie: "2019",
                        seriesNumber: 1,
                        company: "Default",
                        paymentMethod: "NUM",
                        currency: "EUR",
                        documentDate: moment().format(),
                        postingDate: moment().format(),
                        buyerCustomerParty: "Cliente",
                        buyerCustomerPartyName: "ISI_UM",
                        accountingPartyName: "ISI_UM",
                        accountingPartyTaxId: "593362462",
                        exchangeRate: 1,
                        discount: 0,
                        loadingCountry: "PT",
                        unloadingCountry: "PT",
                        isExternal: false,
                        isManual: false,
                        isSimpleInvoice: true,
                        isWsCommunicable: false,
                        deliveryTerm: "V-VIATURA",
                        documentLines,
                        WTaxTotal: {
                            amount: 0,
                            baseAmount: 0,
                            reportingAmount: 0,
                            fractionDigits: 2,
                            symbol: "€"
                        },

                        TotalLiability: {
                            baseAmount: 0,
                            reportingAmount: 0,
                            fractionDigits: 2,
                            symbol: "€"
                        }
                    }
                }
                console.log(JSON.stringify(optionsReq, 0, 3))
                req.post(optionsReq, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })

            }

        ],
        function(err, result) {

            callback(err, result);

        });

}




router.post('/criarFaturaJasmin', function(request, response) {


    faturaJasmin(request.body, (err, result) => {
        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
    })

})






function sendEncomendaHana(options, callback) {

    async.waterfall([

            (done) => {

                var urlOptions = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/encomenda`,
                    json: {
                        id_maquina: options.id_maquina,
                        id_companhia: 69782,
                        id_artigo: options.id_artigo,
                        quantidade: options.quantidade,

                    },

                }

                req.post(urlOptions, (err, body, res) => {

                    var prettyRes = res;
                    done(err, prettyRes);

                })

            }

        ],
        function(err, result) {

            callback(err, result);

        });

}
router.post('/sendEncomendaHana', function(request, response) {

    sendEncomendaHana(request.body, (err, res) => {

        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(res);
        }
    });
});


function sendRotaHana(optionsReq, callback) {

    async.waterfall([

            (done) => {

                getLocationHana(optionsReq, (err, result) => {

                    let latitude = result.latitude;
                    let longitude = result.longitude

                    done(err, latitude, longitude);


                })
            },

            (latitude, longitude, done) => {
                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,
                    json: {
                        id_maquina: optionsReq.id_maquina,
                        id_companhia: 69782,
                        latitude: latitude,
                        longitude: longitude,
                        id_artigo: optionsReq.id_artigo,
                        quantidade: optionsReq.quantidade,
                        estado: optionsReq.estado,
                    },
                }
                req.post(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })

            }

        ],
        function(err, result) {


            callback(err, result);

        });
}


router.post('/sendRotaHana', function(request, response) {

    sendRotaHana(request.body, (err, result) => {
        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
    })

})



function getLocationHana(optionsReq, callback) {

    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/localizacao`,


                }

                req.get(options, (err, body, res) => {
                    let info;
                    let id_maquina = optionsReq.id_maquina;
                    res = JSON.parse(res);
                    for (var a = 0; a < res.length; a++) {
                        if (id_maquina == res[a].id_maquina) {
                            info = res[a];

                            done(err, info);
                        }
                    }
                })
            }
        ],
        function(err, result) {

            callback(err, result);
        });

}


router.post('/getLocationHana', function(request, response) {

    getLocationHana(request.body, (err, result) => {
        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
    })
})




function getOneMoloni(options, callback) {

    async.waterfall([

            (done) => {

                mainModel.getCredentials((err, rows) => {
                    if (err) console.log(err);
                    done(err, options, rows.access_token);
                });
            },

            (options, access, done) => {

                var urlOptions = {
                    url: `https://api.moloni.pt/v1/products/getOne/?access_token=${access}&json=true`,
                    json: {
                        company_id: 69782,
                        product_id: options.id_artigo
                    },

                }
                req.post(urlOptions, (err, body, res) => {


                    var prettyRes = res;
                    done(err, prettyRes);

                })

            }

        ],
        function(err, result) {
            callback(err, result);
        });

}

router.post('/efetuarEncomendaJasmin', function(request, response) {

    getEncomendaHana((err, result) => {

        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
        //console.log(result);
        let product = [];

        var res = JSON.parse(result);
        for (var e of res) {
            product.push({
                product_id: e.id_artigo,
            });
        }

        //console.log(product);


        let encomendas = [];
        let artigos = [];
        const unique = [...new Set(res.map(item => item.id_artigo))];
        //console.log(unique);
        for (var r of unique) {
            let quantity = 0;

            for (var z of res) {

                if (z.quantidade > quantity) {
                    quantity = z.quantidade;
                }

            }

            artigos = {
                id_artigo: r,
                quantidade: quantity
            }
            encomendas.push(artigos);
        }

        let nomes = [];
        let i = 0;
        for (var v of encomendas) {



            getOneMoloni(v, (err, res) => {

                if (err) {
                    console.log(err);
                } else {
                    encomendas[i].nome = res.name;
                    nomes.push(res.name);
                    console.log(v.nome);
                    i++;
                }
                if (i == encomendas.length) {


                    console.log("names array " + JSON.stringify(nomes, 0, 3));
                    console.log("final array " + JSON.stringify(encomendas, 0, 3));

                    faturaJasmin(encomendas, (err, res) => {

                        console.log("erro " + err);
                        console.log("ero " + res);



                    })

                }
            })
            //console.log(nomes);


        }

        //console.log(JSON.stringify(encomendas,0,3))

    });
});


router.post('/efetuarEncomendaHana', function(request, response) {

    getEncomendaHana((err, result) => {

        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
        //console.log(result);
        let product = [];

        var res = JSON.parse(result);
        for (var e of res) {
            product.push({
                product_id: e.id_artigo,
            });
        }

        //console.log(product);


        let encomendas = [];
        let artigos = [];
        const unique = [...new Set(res.map(item => item.id_artigo))];
        //console.log(unique);
        for (var r of unique) {
            let quantity = 0;

            for (var z of res) {

                if (z.quantidade > quantity) {
                    quantity = z.quantidade;
                }

            }

            artigos = {
                id_artigo: r,
                quantidade: quantity
            }
            encomendas.push(artigos);
        }

        let nomes = [];
        let i = 0;
        for (var v of encomendas) {

            getOneMoloni(v, (err, res) => {
                if (err) {
                    console.log(err);
                } else {

                    v.nome = res.name;
                    console.log(v.nome);
                    i++;

                }
                if (i == encomendas.length) {

                    console.log("final array " + JSON.stringify(encomendas, 0, 3));

                    purchaseNAV(encomendas, (err, res) => {

                        console.log("erro " + err);
                        console.log("ero " + res);



                    })

                }
            })
            //console.log(nomes);


        }

        //console.log(JSON.stringify(encomendas,0,3))

    });
});












router.post('/updateRota', function(request, response) {

    async.waterfall([

            (done) => {


                for (var e of request.body) {

                    var options = {
                        url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota/update`,

                        json: {
                            id_artigo: e.id_artigo,
                            id_maquina: e.id_maquina

                        }
                    }

                    console.log(options);
                    req.post(options, (err, body, res) => {
                        console.log(res);

                        var prettyRes = res;

                    })

                }

                done(null, "done");
            }

        ],
        function(err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});


function getEncomendaHana(callback) {

    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/encomenda`,

                }

                req.get(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })
            }
        ],
        function(err, result) {
            callback(err, result);
        });

};


router.get('/getEncomendaHana', function(request, response) {

    getEncomendaHana((err, result) => {

        response.set("Content-Type", "application/json");

        if (err) {
            response.send(err);
        } else {
            response.send(result);
        }
    });
});

router.post('/getRotaHana', function(request, response) {

    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,

                }
                req.get(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })

            }

        ],
        function(err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});

router.post('/getRotaHana', function(request, response) {

    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,

                }
                req.get(options, (err, body, res) => {
                    var prettyRes = res;
                    done(err, prettyRes);
                })

            }

        ],
        function(err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});


router.post('/getRotas', function(request, response) {

    async.waterfall([

            (done) => {

                var options = {
                    url: `https://hanarest1p2001333248trial.hanatrial.ondemand.com/hanarest-1.0/rota`,

                }
                req.get(options, (err, body, res) => {
                    var prettyRes = JSON.parse(res);
                    done(err, body, prettyRes);
                })

            },

            (body, res, done) => {

                let encomenda = [];
                let maqs = [];
                const unique = [...new Set(res.map(item => item.id_maquina))];
                console.log(unique);

                for (var e of unique) {
                    let maq;

                    for (var i of res) {
                        if (i.id_maquina == e) {
                            maq = {

                                latitude: i.latitude,
                                longitude: i.longitude,
                                id_maquina: e
                            }
                        }
                    }
                    maqs.push(maq);
                }
                for (var a of maqs) {
                    let artigos = [];

                    for (var d of res) {
                        if (a.id_maquina == d.id_maquina) {
                            let artigo = {
                                id_artigo: d.id_artigo,
                                quantidade: d.quantidade
                            }
                            artigos.push(artigo);
                        }
                    }
                    const uniqueArt = [...new Set(artigos.map(item => item.id_artigo))];
                    console.log(uniqueArt);
                    let artigosFiltro = [];

                    for (var p of uniqueArt) {
                        let id_artigo = p;
                        let quantity = 0;


                        for (var o of artigos) {
                            if (o.id_artigo == p && o.quantidade > quantity) {
                                quantity = o.quantidade;
                            }
                        }
                        let artigo = {

                            quantidade: quantity,
                            id_artigo: id_artigo
                        }
                        artigosFiltro.push(artigo);

                    }
                    a.artigos = artigosFiltro;
                }
                done(null, maqs);
            }

        ],
        function(err, result) {

            response.set("Content-Type", "application/json");

            if (err) {
                response.send(err);
            } else {
                response.send(result);
            }
        });
});




router.post('/mbway', function(request, response) {
    let params = {
        'chave': chave,
        'valor': request.body.valor,
        'Descrição': 'Compra Chocos',
        'alias': request.body.alias,
        'id': request.body.email
    };
    const date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    req.post({
        url: `${url}/mbway/create`,
        json: true,
        body: params
    }, (err, res, body) => {
        console.log(body);
        if (err || res.statusCode != 200) {
            response.json({
                error: 'An error ocurred Try again later please',
                type: 'ValidationError',
                status: 401
            });
            return;
        }

        let paymentDetails = {
            'referencia': body.referencia,
            'entidade': null,
            'method': 'mbway',
            'user': request.body.email,
            'date_created': date,
        };

        if (body.estado != -9) {
            mainModel.insertPayment(paymentDetails, (err) => {
                if (err) {
                    console.log(err);
                    response.json({
                        error: 'An error ocurred Try again later please',
                        type: 'ValidationError',
                        status: 400
                    });

                    return;
                }





                let timeout = setTimeout(cancel, 60000, 'funky');


                console.log('cancelled');

                function cancel() {

                    clearInterval(interval)
                    response.json({
                        error: 'No ocurr error',
                        type: 'ValidationError',
                        status: 400
                    });




                }

                let interval = setInterval(getPaid, 1500, 'funky');

                function getPaid() {


                    console.log(body.referencia);
                    mainModel.getPaid(body.referencia, (err, result) => {

                        console.log(err);
                        console.log(result);
                        console.log(result[0].trid);

                        if (result[0].trid) {
                            response.json({
                                error: 'No ocurr error',
                                type: 'ValidationError',
                                status: 200
                            });

                            clearInterval(interval)
                            clearTimeout(timeout)
                        }



                    })




                }



            });
        } else {
            response.json({
                error: 'An error ocurred Try again later please',
                type: 'ValidationError',
                status: 400
            });

            return;
        }

    });
});

router.get('/mbCallback', (request, response) => {
    console.log(request.query.referencia + request.query.entidade);

    const date = new Date().toISOString().slice(0, 19).replace('T', ' ');

    function updateMb(params, callback) {
        if (params.entidade < 1) {
            console.log('mw');
            mainModel.updateMW(params, (err) => {
                callback(err);
            });
        } else {
            console.log('mb');
            mainModel.updateMb(params, (err) => {
                callback(err);
            });
        }
    }

    var params = {
        'referencia': request.query.referencia,
        'entidade': request.query.entidade,
    };
    console.log('pago');
    params.date = date;
    params.trid = request.query.transacao;

    updateMb(params, (err) => {
        if (err) console.log(err);
    });
});




module.exports = router;
