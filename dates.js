  var abstractModel = require('../models/abstract.model.js');


  //Update database entry with date it was paid


  //Exporting function so we can call it from server
  module.exports = {
      refresh: (callback) => {
          abstractModel.listDates(function(err, dates) {
              if (err) console.log(err);
              var today = Date.now();
              var nextDate;
              var text;
              if (dates) {
                  for (var i = 0; i < dates.length; i++) {
                      if (dates[i].date > today && nextDate == null) {
                          let date = dates[i].date.toLocaleDateString('pt-PT');
                          nextDate = date;
                          text = dates[i].name;
                      }
                  }
              }
              callback(nextDate, text);
          });

          console.log('finished');
      },

  };
