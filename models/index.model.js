module.exports = {

    getCredentials(callback) {
        var sql = 'SELECT * from credentials'
        global.connection.query(sql, function(err, rows) {
            callback(err, rows[0]);
        });
    },
    updateMologni(data, callback) {
        var sql = 'UPDATE  credentials SET access_token=?, refresh_token=? where id=1 ';
        global.connection.query(sql, [data.access_token, data.refresh_token], function(err, rows) {
            callback(err, rows);
        });
    },
    updateJasmin(data, callback) {
        var sql = 'UPDATE  credentials SET jasmin_access_token = ? where id=1 ';
        global.connection.query(sql, [data.access_token], function(err, rows) {
            callback(err, rows);
        });
    },
    insertPayment(data, callback) {
        let error = null;
        var sql = 'INSERT INTO payments (reference, entity, id_ticket, date_created,method) VALUES (?,?,?,?,?)';
        global.connection.query(sql, [data.referencia, data.entidade, data.user, data.date_created, data.method], function(err, rows) {
            if (err) {
                error = err;
            }
        });
        callback(error);
    },
    getPaid(referencia, callback) {
        let error = null;
        var sql = 'SELECT * from payments where reference=?';
        global.connection.query(sql, [referencia], (err, rows) => {
            if (err)
                error = err;
            callback(error, rows);
        });
    },

    updateMW(data, callback) {
        let error = null;
        var sql = 'UPDATE  payments set date_payed=?, trid=?  where reference=?';
        global.connection.query(sql, [data.date, data.trid, data.referencia], (err, rows) => {
            if (err)
                error = err;
            callback(error, rows);
        });
    },
    updateMb(data, callback) {
        let error = null;
        var sql = 'UPDATE  payments set date_payed=?, trid=?  where entity=? and reference=?';
        global.connection.query(sql, [data.date, data.trid, data.entidade, data.referencia], (err, rows) => {
            if (err)
                error = err;
            callback(error, rows);
        });
    },

};
