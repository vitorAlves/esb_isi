/* eslint-disable no-undef */ /* eslint-disable no-console */
const port = 3000;
const express = require('express');

const mysql = require('mysql');
const bodyParser = require('body-parser');
const app = express();
const validator = require('express-validator');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');

const cron = require('node-cron');
var nodemailer = require('nodemailer');
const jasmin = require('./workers/jasmin.js');
const mologni = require('./workers/mologni.js');
require('custom-env').env('staging');

const Store = require('data-store');
global.store = new Store({
    path: 'config.json'
});

global.secure = function(type) {
    return function(request, response, next) {
        if (request.isAuthenticated()) {
            if (!type) {
                return next();
            } else if (type == "reviewer" && (request.user.type == "reviewer" || request.user.type == "admin")) {

                return next();

            } else if (type == "admin" &&  request.user.type == "admin") {
                return next();
            } else {
                response.redirect(request.originalUrl);

            }
        } else {
            request.session.returnTo = request.originalUrl;
            // eslint-disable-next-line no-console
            response.redirect('/login');
        }
    };
};

// eslint-disable-next-line no-undef
var RedisStore = require('connect-redis')(session);
var options = {
    port: process.env.REDIS_PORT
}

app.use(session({
    store: new RedisStore(options),
    saveUninitialized: true,
    secret: 'keyboard cat',
    resave: false
}));

//This function will allow us to retrict the access to the routes
//end of 
app.use(validator());
app.use(bodyParser.json({
    limit: '24mb'
}), bodyParser.urlencoded({
    extended: true
}));

//new
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(username, callback) {
    callback(null, username);
});

passport.deserializeUser((username, callback) => {
    model.read(username, (err, data) => {
        callback(null, data);
    });

});




app.set('view engine', 'ejs');
app.set('views', 'views');
global.connection = mysql
    .createPool({
        connectionLimit: 100,
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB
    });





var server = app.listen(port, function() {
    console.log(`worker ${process.pid}`);
});



server.keepAlive = 8000;

//cron.schedule("*/30 * * * * *", () => {
	//jasmin.refresh();
	//mologni.refresh();
//});




app.use('/', require('./controllers/index.route'));
app.use('/public', express.static('public', {
    maxAge: 86400000
}));

